package com.thanh.dao;


import com.thanh.model.Account;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class AccountDaoImpl implements AccountDao {

    private static final org.apache.log4j.Logger logger = Logger.getLogger(AccountDaoImpl.class);
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @SuppressWarnings("unchecked")
    @Override
    public Account CheckAccount(String username, String psw) {
        Session session = this.sessionFactory.getCurrentSession();
        logger.info("check login :" + username );
        Account account=null;
        try {
            account = (Account) session.createQuery("from Account where username ='"+ username +"' and psw ='"+psw+"'").getSingleResult();
            return account;
        }catch (NoResultException nre){
            logger.debug(nre);
         return account;
        }
    }

}
