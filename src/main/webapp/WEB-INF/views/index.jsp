<%--
  Created by IntelliJ IDEA.
  User: masterenit
  Date: 15/10/2019
  Time: 17:10
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<html>
<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.min.js"></script>

</head>

<style>
    img {
        height: 200px;
        width: 200px;
    }
</style>
<body>

<script type="text/javascript">


    // $(function () {
    //   $("#search").click(function() {
    //     var name = document.getElementById("name").value;
    //     $.get('/search-name?namebook=' +name,function (data) {
    //     });
    //
    //   });
    //
    // })
    $(function () {
        $("#name").keyup(function (event) {
            if (event.keyCode === 13) {
                $("#search").click();
            }
        });

        $("#search").click(function () {

            var name = document.getElementById("name").value;
            $.ajax({
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                data: {
                    'namebook': name
                },
                // headers: {
                //   'Accept':'application/json'
                // },
                url:'${pageContext.request.contextPath}' + '/search-name',
                success: function (result) {
                    console.log(result);
                    $("#tableProductList").html("");
                    $.each(result, function (index, sp) {
                        var row = ' <div class="col">' + ' <div class="f_p_item"> '
                            + '<div class="f_p_img">'
                            + '<img class="img-fluid" src="' + '${pageContext.request.contextPath}' + sp.img + '" alt="">'
                            + '</div>'
                            + '<a href="' + '${pageContext.request.contextPath}' + '/single-product?id=' + sp.id + '">'
                            + '<h4>' + sp.name
                            + '</h4>'
                            + '</a>'
                            + '</div>'
                            + '</div>';
                        $("#tableProductList").append(row);
                    })
                }
            });
        });
    })


</script>

<c:set var="currentUser" value="${sessionScope.accountlogin}"/>

<div class="container" style="float: left">
    <c:if test="${empty currentUser}">
        <h1><a href="${pageContext.request.contextPath}/login" style="border: 0;">Login</a></h1>
    </c:if>
    <c:if test="${not empty currentUser}">
        <h1><a href="${pageContext.request.contextPath}/logout" style="border: 0;">logout</a></h1>
    </c:if>
</div>

<div class="left_dorp">
    <label>Search </label>
    <input type="text" id="name" class="inputname" name="name" style="margin-left: 10px" placeholder="Enter name book "
           required=""/>
    <button id="search" class="icons" style="cursor: pointer; margin-left: 10px"><i class="fa fa-search"
                                                                                    aria-hidden="true"></i></button>
</div>
<section class="feature_product_area section_gap">
    <div class="main_box">
        <div class="container-fluid">
            <div class="row">
                <div class="main_title">
                    <h2>All Products</h2>
                </div>
            </div>
            <div class="row" id="tableProductList">
                <c:forEach var="product" items="${listbook}">
                    <div class="col">
                        <div class="f_p_item">
                            <div class="f_p_img">
                                <img class="img-fluid" src="${pageContext.request.contextPath}${product.img}" alt="">
                            </div>
                            <a href="${pageContext.request.contextPath}/single-product?id=${product.id}">
                                <h4>${product.name}</h4>
                            </a>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</section>
</body>
</html>
