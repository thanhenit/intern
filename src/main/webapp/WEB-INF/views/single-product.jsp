<%--
  Created by IntelliJ IDEA.
  User: masterenit
  Date: 17/10/2019
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Single Product</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.min.js"></script>
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<style>
    .carousel-indicators li {
        text-indent: 0px !important;
    }

    .viewproduct {
        padding-top: 100px;
    }
</style>
<body>
<c:set var="currentUser" value="${sessionScope.accountlogin}"/>
<div class="container" style="float: left">
    <c:if test="${empty currentUser}">
        <h1><a href="${pageContext.request.contextPath}/login" style="border: 0;">Login</a></h1>
    </c:if>
    <c:if test="${not empty currentUser}">
        <h1><a href="${pageContext.request.contextPath}/logout" style="border: 0;">logout</a></h1>
    </c:if>
</div>
<div style="float: right" class="contrainer">
    <h1><a href="${pageContext.request.contextPath}/" style="border: 0;">Home</a></h1>
</div>
<section class="viewproduct">
    <div class="container">
        <div class="row s_product_inner">
            <div class="col-lg-6">
                <div class="s_product_img">
                    <div id="carouselExampleIndicators" class="carousel slide"
                         data-ride="carousel">
                        <c:if test="${not empty listimage}">


                        <!-- Small image -->
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                class="active" style="height: 60px; width: 60px;"><img
                                    src="${pageContext.request.contextPath}${listimage.get(0)}" alt=""
                                    style="height: 60px; width: 60px;"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"
                                style="height: 60px; width: 60px;"><img
                                    src="${pageContext.request.contextPath}${listimage.get(1)}" alt=""
                                    style="height: 60px; width: 60px;"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"
                                style="height: 60px; width: 60px;"><img
                                    src="${pageContext.request.contextPath}${listimage.get(2)}" alt=""
                                    style="height: 60px; width: 60px;"></li>
                        </ol>

                        <!-- Large image -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="${pageContext.request.contextPath}${listimage.get(0)}"
                                     alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="${pageContext.request.contextPath}${listimage.get(1)}"
                                     alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="${pageContext.request.contextPath}${listimage.get(2)}"
                                     alt="Third slide">
                            </div>
                        </div>
                        </c:if>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 offset-lg-1">
                <div class="s_product_text">
                    <a href="##"><h1>${book.name}</h1></a>
                    <h1><a href="##">${book.author}</a></h1>
                    <h1>Description</h1>
                    <h3>${book.description}</h3>


                </div>
            </div>

            <a class="btn btn-primary" href="${pageContext.request.contextPath}/view?id=${book.id}">Read Book</a>
            <a style="margin-left: 320px;" class="btn btn-info" href="${pageContext.request.contextPath}${pdf}"
               download>Download Book</a>

        </div>
    </div>
</section>

<section class="container">
    <div class="tab-pane fade show active" id="review" role="tabpanel"
         aria-labelledby="review-tab">
        <div class="row">
            <div class="col-lg-6">
                <div class="row total_rate">
                    <div class="col-6">
                        <div class="box_total">
                            <h5>Overall</h5>
                            <h4>${showscore}</h4>
                            <h6>
                                (<span>&nbsp;${countrt}&nbsp;</span> Reviews)
                            </h6>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="rating_list">
                            <h3>
                                Based on <span>&nbsp;${countrt}&nbsp;</span> Reviews
                            </h3>
                            <ul class="list" style="color: #fbd600;">

                                <c:forEach items="${showratingid}" var="rtt"
                                           varStatus="status">


                                    <c:if test="${rtt.score>=5}">
                                        <li><i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                    class="fa fa-star"></i></li>
                                    </c:if>
                                    <c:if test="${rtt.score==4}">
                                        <li><i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i></li>
                                    </c:if>
                                    <c:if test="${rtt.score==3}">
                                        <li><i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i></li>
                                    </c:if>
                                    <c:if test="${rtt.score==2}">
                                        <li><i class="fa fa-star"></i> <i class="fa fa-star"></i></li>
                                    </c:if>
                                    <c:if test="${rtt.score==1}">
                                        <li><i class="fa fa-star"></i></li>
                                    </c:if>
                                </c:forEach>

                            </ul>
                        </div>
                    </div>
                </div>
                <c:forEach items="${showratingid}" var="rt" varStatus="status">
                    <div class="review_list">
                        <div class="review_item">
                            <div class="media">
                                <div class="d-flex">
                                    <img
                                            src="${pageContext.request.contextPath}/resources/img/logouser.jpg"
                                            alt="" style="height: 70px; width: 71px;">
                                </div>
                                <div class="media-body">
                                    <p>${rt.date}</p>
                                    <h4>${rt.email}</h4>

                                    <c:if test="${rt.score==5}">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </c:if>
                                    <c:if test="${rt.score==4}">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </c:if>
                                    <c:if test="${rt.score==3}">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </c:if>
                                    <c:if test="${rt.score==2}">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </c:if>
                                    <c:if test="${rt.score==1}">
                                        <i class="fa fa-star"></i>
                                    </c:if>

                                </div>
                            </div>
                            <p>${rt.comment}</p>

                        </div>


                    </div>
                </c:forEach>

            </div>
            <div class="col-lg-6">
                <div class="review_box">
                    <h4>Add a Review</h4>
                    <p>Your Rating:</p>
                    <ul class="list">
                        <div class="container">
                            <div class="row lead">
                                <div id="stars-existing" class="starrr" data-rating='4'
                                     style="color: #ffca08;"></div>

                                You gave a rating of <span> &nbsp; </span> <span
                                    id="count-existing"> 4 </span> <span> &nbsp; </span> star(s)
                            </div>
                        </div>
                    </ul>
                    <p>Outstanding</p>
                    <div class="row contact_form" id="contactForm">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="email" class="form-control" id="emailaddress"
                                       name="emailaddress" placeholder="Email Address">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
											<textarea class="form-control" name="comment" id="comment"
                                                      rows="1" placeholder="Review"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn submit_btn btnsubmit"
                                    id="btnsubmittt">Submit Now
                            </button>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</section>
<script src="${pageContext.request.contextPath}/resources/js/review.js"></script>

</body>
</html>
