package com.thanh.service;

import com.thanh.model.Book;
import com.thanh.model.Book_img;

import java.util.List;

public interface BookService {
    List<Book> showlistbook();

    void addbook(Book b);

    void updateBook(Book b);

    void rmBook(int id);

    List<Book> selectBookName(String name);

    List<String> listimg(int id);

    Book b(int id);

    void addimg(Book_img bimg);

    Book selectlastid();

    String linkpdf(int id);

    void rmImgBook(int book_id);

    Book_img thelastimg();
}
