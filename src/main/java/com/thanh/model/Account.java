package com.thanh.model;


import javax.persistence.*;

@Entity
@Table
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String psw;

    public Account(){
        super();
    }
    public Account(String username,String psw){
        super();
        this.username=username;
        this.psw=psw;
    }

    public int getId() {
        return id;
    }

    public String getPsw() {
        return psw;
    }

    public String getUsername() {
        return username;
    }
}
