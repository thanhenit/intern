<%--
  Created by IntelliJ IDEA.
  User: masterenit
  Date: 17/10/2019
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Single Product</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>

</head>
<style>
    .carousel-indicators li {
        text-indent: 0px !important;
    }

    .viewproduct {
        padding-top: 200px;
    }
</style>
<body>
<c:set var="currentUser" value="${sessionScope.accountlogin}"/>
<div class="container" style="float: left">
    <c:if test="${empty currentUser}">
        <h1><a href="${pageContext.request.contextPath}/login" style="border: 0;">Login</a></h1>
    </c:if>
    <c:if test="${not empty currentUser}">
        <h1><a href="${pageContext.request.contextPath}/logout" style="border: 0;">logout</a></h1>
    </c:if>
</div>
<div style="float: right" class="contrainer">
    <h1><a href="${pageContext.request.contextPath}/" style="border: 0;">Home</a></h1>
</div>
<section class="viewproduct">
    <div class="container">
        <div class="row s_product_inner">
            <div class="col-lg-6">
                <div class="s_product_img">
                    <div id="carouselExampleIndicators" class="carousel slide"
                         data-ride="carousel">

                        <!-- Small image -->
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                class="active" style="height: 60px; width: 60px;"><img
                                    src="${pageContext.request.contextPath}${listimage.get(0)}" alt=""
                                    style="height: 60px; width: 60px;"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"
                                style="height: 60px; width: 60px;"><img
                                    src="${pageContext.request.contextPath}${listimage.get(1)}" alt=""
                                    style="height: 60px; width: 60px;"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"
                                style="height: 60px; width: 60px;"><img
                                    src="${pageContext.request.contextPath}${listimage.get(2)}" alt=""
                                    style="height: 60px; width: 60px;"></li>
                        </ol>

                        <!-- Large image -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="${pageContext.request.contextPath}${listimage.get(0)}"
                                     alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="${pageContext.request.contextPath}${listimage.get(1)}"
                                     alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="${pageContext.request.contextPath}${listimage.get(2)}"
                                     alt="Third slide">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 offset-lg-1">
                <div class="s_product_text">
                    <a href="##"><h1>${book.name}</h1></a>
                    <h1><a href="##">${book.author}</a></h1>
                    <h1>Description</h1>
                    <h3>${book.description}</h3>


                </div>
            </div>

            <a class="main_btn" href="${pageContext.request.contextPath}/view?id=${book.id}">Read Book</a>

        </div>
    </div>
</section>
</body>
</html>
