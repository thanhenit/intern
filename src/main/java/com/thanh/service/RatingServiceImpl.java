package com.thanh.service;


import com.thanh.dao.RatingDao;
import com.thanh.model.Rating;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {
    private RatingDao ratingDao;

    public void setRatingDao(RatingDao ratingDao) {
        this.ratingDao = ratingDao;
    }

    @Override
    @Transactional
    public List<Integer> slRatingUser(int account_id) {
        return this.ratingDao.slRatingforUser(account_id);
    }

    @Override
    @Transactional
    public void insertRating(Rating rating) {
        this.ratingDao.insertRating(rating);
    }

    @Override
    @Transactional
    public List<Rating> showRatingforid(int idProduct) {
        return this.ratingDao.showRatingforid(idProduct);
    }

    @Override
    @Transactional
    public List<Rating> slRating_id_and_name(Rating rating) {
        return this.ratingDao.slRating_id_and_name(rating);
    }

    @Override
    @Transactional
    public void updateRating(Rating rating) {
        this.ratingDao.updateRating(rating);
    }

    @Override
    @Transactional
    public Long countRating(int idProduct) {
        return this.ratingDao.countRating(idProduct);
    }

    @Override
    @Transactional
    public Long totalScore(int idProduct) {
        return this.ratingDao.totalScore(idProduct);
    }
}
