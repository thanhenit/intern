<%--
  Created by IntelliJ IDEA.
  User: masterenit
  Date: 15/10/2019
  Time: 17:10
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.min.js"></script>
</head>

<style>
    img {
        height: 100px;
        width: 100px;
    }
</style>
<body>
<c:set var="currentUser" value="${sessionScope.accountlogin}"/>

<script type="text/javascript">

    var currentId;
    $(function () {
        $(".editid").click(function () {
            var saveNum = $(this).parents("tr").attr("data-num");
            currentId = saveNum;
            $.ajax({
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                data: {
                    'id': currentId
                },
                url: '/admin/getid',
                success: function (result) {
                    console.log(result)
                    $("#tablebook").html("")
                    var row = ' <p>name</p>'
                        + '  <input type="text" id="nameedit" name="nameedit" required=""  value="' + result.name + '" }> '
                        + ' <p>author</p>'
                        + '  <input type="text" id="authoredit" name="authoredit" required=""  value="' + result.author + '" }> '
                        + ' <p>year</p>'
                        + '  <input type="date" id="yearedit" name="yearedit" required=""  value="' + result.year + '" }  style="width: 205px;" > '
                    $("#tablebook").append(row);
                }
            });
        });
    })
    $(function () {
        $(".delid").click(function () {
            var saveNum = $(this).parents("tr").attr("data-num");
            currentId = saveNum;

        });
    })


    $(function () {
        $(".btnYesdel").click(function () {
            $.get('/admin/delbook?id=' + currentId, function (data) {
                location.reload();

            });

        });

    })

    $(function () {
        $(".editbook").click(function () {
            alert(currentId);
            var name = document.getElementById("nameedit").value;
            var author = document.getElementById("authoredit").value;
            var year = document.getElementById("yearedit").value;
            $.get('/admin/editbook?id=' + currentId + '&author=' + author + '&year=' + year + '&name=' + name, function (data) {
                location.reload();

            });

        });

    })

    var loadFile2 = function (event) {
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('output2');
            output.src = reader.result;
            console.log(reader);
            console.log(output);
        };
        reader.readAsDataURL(event.target.files[0]);
    };


</script>

<form name="sendData" id="sendData">
    <input type="hidden" name="num" id="num">
</form>


<c:if test="${not empty currentUser}">
    <h1><a href="${pageContext.request.contextPath}/logout" style="border: 0;">logout</a></h1>
</c:if>
<section class="lala">
    <div class="container">

        <table class="table">
            <thead>
            <tr>
                <th scope="col"> Name</th>
                <th scope="col"> Author</th>
                <th scope="col"> Year</th>
                <th scope="col"> Img</th>
                <th scope="col"> Description</th>
                <th scope="col"> Read</th>

            </tr>
            <tbody>
            <c:if test="${not empty listbook}">
                <c:forEach items="${listbook}" var="list" varStatus="status">
                    <tr data-num="${list.id}">
                        <td>${list.name}</td>
                        <td>${list.author}</td>
                        <td>${list.year}</td>
                        <td><img src="${pageContext.request.contextPath}${list.img}"></td>
                        <td>${list.description}</td>
                        <td><a href="${pageContext.request.contextPath}/view?id=${list.id}"> View</a></td>
                        <td><a class="btn btn-warning editid" data-toggle="modal"
                               data-target="#exampleModaledit">Edit</a></td>
                        <td><a class="btn btn-danger delid" data-toggle="modal"
                               data-target="#exampleModaldel">Delete</a></td>
                    </tr>

                </c:forEach>
            </c:if>
            </tbody>
            </thead>

        </table>

        <a href="${pageContext.request.contextPath}/admin/add" style="float: right;" class="btn btn-primary">Add</a>

        <%--        <div class="modal fade" id="exampleModaladd" tabindex="-1"--%>
        <%--             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--%>
        <%--            <div class="modal-dialog" role="document">--%>
        <%--                <div class="modal-content">--%>
        <%--                    <div class="modal-header">--%>
        <%--                        <h5 class="modal-title">add book</h5>--%>
        <%--                        <button type="button" class="close" data-dismiss="modal"--%>
        <%--                                aria-label="Close">--%>
        <%--                            <span aria-hidden="true">&times;</span>--%>
        <%--                        </button>--%>
        <%--                    </div>--%>
        <%--                    <div class="modal-body">--%>
        <%--                        <form method="post" action="${pageContext.request.contextPath}/admin/editbook">--%>
        <%--                            <div class="form-group ">--%>
        <%--                                <p>name</p>--%>
        <%--                                <input type="text" id="nameadd" name="nameadd" required="">--%>
        <%--                                <p>author</p>--%>
        <%--                                <input type="text" id="authoradd" name="authoradd" required="">--%>
        <%--                                <p>year</p>--%>

        <%--                            </div>--%>
        <%--                            <div class="modal-footer">--%>
        <%--                                <button type="button" class="btn btn-secondary"--%>
        <%--                                        data-dismiss="modal">Cancel--%>
        <%--                                </button>--%>
        <%--                                <button type="submit" class="btn btn-primary ">Accept</button>--%>
        <%--                            </div>--%>


        <%--                        </form>--%>
        <%--                    </div>--%>

        <%--                </div>--%>
        <%--            </div>--%>
        <%--        </div>--%>


        <div class="modal fade" id="exampleModaldel" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Clothes</h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">Do you Want ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-lg"
                                data-dismiss="modal">No
                        </button>
                        <button id="btnYes" type="button"
                                class="btn btn-danger btn-lg btnYesdel">Yes
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <form action="${pageContext.request.contextPath}/admin/editbook" method="post"
              enctype="multipart/form-data">
            <div class="modal fade" id="exampleModaledit" tabindex="-1"
                 role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit book</h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post">
                                <div class="form-group ">
                                    <div id="tablebook">
                                        <p>name</p>
                                        <input type="text" id="nameedit" name="nameedit" required="" }>
                                        <p>author</p>
                                        <input type="text" id="authoredit" name="authoredit"
                                        required="">

                                        <p>year</p>
                                        <input type="date" id="yearedit" name="yearedit"
                                               required="" style="width: 205px;">
                                    </div>
                                    <p>Description</p>
                                    <textarea name="descriptionedit" id="descriptionedit" rows="10"
                                              placeholder="Description"></textarea>
                                    <p>Image</p>
                                    <input id="file2" name="file2" class="input-file"
                                           type="file" onchange="loadFile2(event)"
                                           accept="image/gif, image/jpeg, image/png">
                                    <div>
                                        <img id="output2" src="#" style="height: 100px;width: 100px;"/>
                                    </div>
                                    <div>
                                        <p>PDF File</p>
                                        <input id="fileppdf" name="filepdf" class="input-file"
                                               type="file" accept="application/pdf">
                                        <div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary" aria-label="Close">Confirm
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
</body>
</html>
