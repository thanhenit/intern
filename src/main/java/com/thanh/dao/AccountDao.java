package com.thanh.dao;

import com.thanh.model.Account;

public interface AccountDao {
    Account CheckAccount(String username, String psw);
}
