package com.thanh.dao;

import  org.apache.commons.lang3.StringUtils;
import com.thanh.model.Book;
import com.thanh.model.Book_img;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookDaoImpl implements BookDao {
    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(BookDaoImpl.class);

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Book> listbook() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Book> booklist = session.createQuery("from Book").list();
        return booklist;
    }

    @Override
    public void addbook(Book b) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(b);
    }

    @Override
    public void updateBook(Book b) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(b);

    }

    @Override
    public void rmBook(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Book b = session.load(Book.class, new Integer((id)));
        if (null != b) {
            session.delete(b);
        }

    }

    @Override
    public List<Book> selectBookName(String name) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Book> bookList;
        //check space input return true or false
        boolean a = StringUtils.trim(name).isEmpty();
        if(name ==null || a==true){
            System.out.println("aaaa");
            bookList=session.createQuery("from Book").list();
        }else {
            System.out.println("bbbb");
            bookList = session.createQuery("from Book where name like concat('%','" + name + "','%') ").list();

        }

        return bookList;
    }

    @Override
    public List<String> listimg(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        List<String> listimg = null;
        try {
           listimg = session.createQuery("select img from  Book_img where Book_id='" + id + "'").list();
         logger.info("select img book : " + id);
            return listimg;

        }catch (Exception e){
           logger.error(e);
            return listimg;
        }

    }

    @Override
    public Book b(int id) {

        Session session = this.sessionFactory.getCurrentSession();
        Book b = null;
        try {
            b = (Book) session.createQuery("from  Book where id='" + id + "'").getSingleResult();
             logger.info("select book : " + b.getName());
            return b;

        }catch (Exception e){
          logger.error(e);
            return b;
        }
    }

    @Override
    public void addimg(Book_img bimg) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(bimg);
    }

    @Override
    public Book SelectLastBook() {
        Session session = this.sessionFactory.getCurrentSession();
        Book b = new Book();
        try {
           b = (Book) session.createQuery("FROM Book order by id desc ").setMaxResults(1).uniqueResult();
        }catch (Exception e){
            logger.error("error select the last book" + e.getMessage());
        }

        return b;
    }

    @Override
    public String linkpdf(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        String pdf=null;
        try{
           pdf = (String) session.createQuery("SElECT pdf FROM Book where id='" + id + "'").getSingleResult();
        }catch (Exception e){
            System.out.println(e.getMessage());
            logger.error("get llink pdf fail " +e.getMessage() );
        }
        return pdf;
    }

    @Override
    public void updateBookImg(Book_img bing) {
        Session session = this.sessionFactory.getCurrentSession();
        try{
            List<Book_img> bImg = session.createQuery("FROM Book_img where Book_id='"+ bing.getBook_id()+"'").list();
            session.update(bImg);
        }catch (Exception e){
            logger.error("update book img fail" +e.getMessage());
        }
        session.update(bing);
    }

    @Override
    public void rmImgBook(int Book_id) {
        Session session = this.sessionFactory.getCurrentSession();
        System.out.println(Book_id);
        try{
            List<Book_img> bImg = session.createQuery("FROM Book_img where Book_id='"+Book_id+"'").list();
            for(Book_img a : bImg){
                session.delete(a);
            }


        }catch (Exception e){
         logger.error("rm img book"+ e.getMessage());
        }
    }

    @Override
    public Book_img theLastImg() {
        Session session = this.sessionFactory.getCurrentSession();
        Book_img bimg = new Book_img();
        try {
            bimg = (Book_img) session.createQuery("FROM Book_img order by id desc ").setMaxResults(1).uniqueResult();
        }catch (Exception e){
            logger.error("the last img "+ e.getMessage());
        }
        return bimg;
    }



}
