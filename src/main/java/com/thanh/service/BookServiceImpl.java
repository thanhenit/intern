package com.thanh.service;

import com.thanh.dao.BookDao;
import com.thanh.model.Book;
import com.thanh.model.Book_img;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private BookDao bookDao;

    public void setBookdao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    @Transactional
    public List<Book> showlistbook() {
        return this.bookDao.listbook();
    }

    @Override
    @Transactional
    public void addbook(Book b) {
        this.bookDao.addbook(b);
    }

    @Override
    @Transactional
    public void updateBook(Book b) {
        this.bookDao.updateBook(b);
    }

    @Override
    @Transactional
    public void rmBook(int id) {
        this.bookDao.rmBook(id);
    }

    @Override
    @Transactional
    public List<Book> selectBookName(String name) {
        return this.bookDao.selectBookName(name);
    }

    @Override
    @Transactional
    public List<String> listimg(int id) {
        return this.bookDao.listimg(id);
    }

    @Override
    @Transactional
    public Book b(int id) {
        return this.bookDao.b(id);
    }

    @Override
    @Transactional
    public void addimg(Book_img bimg) {
        this.bookDao.addimg(bimg);
    }

    @Override
    @Transactional
    public Book selectlastid() {
        return this.bookDao.SelectLastBook();
    }

    @Override
    @Transactional
    public String linkpdf(int id) {
        return this.bookDao.linkpdf(id);
    }

    @Override
    @Transactional
    public void rmImgBook(int book_id) {
        this.bookDao.rmImgBook(book_id);
    }

    @Override
    @Transactional
    public Book_img thelastimg() {
        return this.bookDao.theLastImg();
    }
}
