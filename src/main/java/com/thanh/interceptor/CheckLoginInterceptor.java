package com.thanh.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CheckLoginInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        boolean conditions = false;
        HttpSession session = request.getSession();

        if (session.getAttribute("accountlogin") == null) {
            conditions=true;

        } else {

            response.sendRedirect(request.getContextPath() + "/");
        }
        return conditions;
    }
}
