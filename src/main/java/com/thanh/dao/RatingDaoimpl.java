package com.thanh.dao;


import com.thanh.model.Rating;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RatingDaoimpl implements RatingDao {

    private static final org.apache.log4j.Logger logger = Logger.getLogger(RatingDaoimpl.class);
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Integer> slRatingforUser(int account_id) {
        System.out.println("**********************");
        Session session = this.sessionFactory.getCurrentSession();
        List<Integer> listaccount = null;
        try {
            listaccount = session.createQuery("select account_id from  Rating where account_id='" + account_id + "'").list();
            logger.info("**********************" +listaccount.size());
            return listaccount;

        }catch (Exception e){
            logger.error(e.getMessage() + "  error sl rating user");
            return listaccount;
        }

    }

    @Override
    public void insertRating(Rating rating) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(rating);
    }

    @Override
    public List<Rating> showRatingforid(int idProduct) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Rating> listrating=null;
        try {
           listrating=session.createQuery("from Rating where book_id='" + idProduct + "'").list();
           return listrating;
        }catch (Exception e){
            logger.error(e.getMessage() + "  error show rating id");
        }
        return listrating;
    }

    @Override
    public List<Rating> slRating_id_and_name(Rating rating) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Rating> listrating=null;
        try {
            listrating=session.createQuery("from Rating where book_id='" + rating.getBook_id() + "' AND account_id='"+rating.getAccount_id()+"'").list();
            return listrating;
        }catch (Exception e){
            logger.error(e.getMessage() + "  error sl rating user and name");
        }
        return listrating;
    }

    @Override
    public void updateRating(Rating rating) {
        Session session = this.sessionFactory.getCurrentSession();
        logger.info("update rating"+rating.getEmail()+"===="+rating.getComment()+"===="+rating.getScore()+"===="+rating.getBook_id()+"===="+rating.getAccount_id());
        try {
            Query a = session.createQuery("UPDATE Rating Set email='"+rating.getEmail()+"',comment='"+rating.getComment()+"',date='"+rating.getDate()+"',score='"+rating.getScore()+"' where book_id='" + rating.getBook_id() + "' AND account_id='"+rating.getAccount_id()+"'");
          int result=a.executeUpdate();
        }catch (Exception e){
            logger.error(e.getMessage() + "  error update rating");
        }

    }

    @Override
    public Long countRating(int idProduct) {
        Session session = this.sessionFactory.getCurrentSession();
        Long result = null;
        try {
          Query a = session.createQuery("select count (*) from Rating where book_id='"+idProduct+"'");
           result = (Long) a.uniqueResult();
        }catch (Exception e){
            logger.error(e);
        }

        return result;
    }

    @Override
    public Long totalScore(int idProdcut) {
        Session session = this.sessionFactory.getCurrentSession();
        Long result = null;
        try {
            Query a = session.createQuery("select SUM(score) from Rating where book_id='"+idProdcut+"'");
            result = (Long) a.uniqueResult();
        }catch (Exception e){
            logger.error(e);
        }

        return result;
    }
}
