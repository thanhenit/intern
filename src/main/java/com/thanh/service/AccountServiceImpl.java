package com.thanh.service;


import com.thanh.dao.AccountDao;
import com.thanh.model.Account;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountServiceImpl  implements AccountService{
    private AccountDao accountDao;

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    @Transactional
    public Account checklogin(String username, String psw) {

        return this.accountDao.CheckAccount(username,psw);
    }
}
