package com.thanh.Controller;

import com.thanh.dao.AccountDao;
import com.thanh.dao.RatingDao;
import com.thanh.model.*;
import com.thanh.service.AccountService;
import com.thanh.service.BookService;

import com.thanh.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Controller
public class homecontroller {

    BookService bookService;

    @Inject
    PDF pdff;

    private static final Logger logger = Logger.getLogger(homecontroller.class);

    private static int idbook;

    private static int idpdf;

    private static int idProduct;

    private static int check = 0;

    private static int safeiddel;


    @Inject
    AccountService accountService;


    @Inject
    RatingService ratingService;

    @Autowired
    private ApplicationContext context;

    @Autowired(required = true)
    @Qualifier(value = "bookService")

    public void setBookService(BookService bs) {
        this.bookService = bs;
    }

    // request " / " return jsp index
    @RequestMapping(value = "/")
    public String index(Model model) {
        model.addAttribute("listbook", this.bookService.showlistbook());
        return "index";
    }

    @RequestMapping(value = "/login")
    public String login(Model model) {
        return "login";
    }


    @RequestMapping(value = "/signin")
    public String sigin(Model model, HttpServletRequest request, HttpSession session) {
        // call session
        session = request.getSession();
        // get param uname && psw
        String username = request.getParameter("uname");
        String psw = request.getParameter("psw");
        // check account return model account
        Account account = accountService.checklogin(username, psw);
        if (username.equals("") || psw.equals("")) {
            // if fail return page login
            return "login";
        }
        if (account != null) {
            // if  account not null (account exist)
            //  setAtribute session account login
            session.setAttribute("accountlogin", account);
            if (username.equals("admin")) {
                // check session account
                // if account name admin => return page admin
                logger.info("login : " + username);
                return "redirect:/admin";
            }
            // if account not admin => return page index
            logger.info("login : " + username);
            return "redirect:/";

        } else {
            // account null => (Do not have an account) => return page login (Login again)
            logger.error("login fail");
            return "login";
        }

    }

    @RequestMapping(value = "/logout")
    public String signout(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) {
        session = request.getSession();
        // call session && invalidate session
        session.invalidate();
        return "redirect:/";
    }


    @RequestMapping(value = "/admin")
    public String listbook(Model model) {
        Book b = null;
        //select and show list book
        model.addAttribute("listbook", this.bookService.showlistbook());
        model.addAttribute("infobook", b);
        return "admin";
    }


    @RequestMapping(value = "/admin/editbook")
    public String editbook(HttpServletRequest request, Model model, @RequestParam(value = "file2") CommonsMultipartFile commonsMultipartFiles, @RequestParam(value = "filepdf") CommonsMultipartFile pdffile) throws IOException {
        // get name file insert
        String nameFile2 = commonsMultipartFiles.getOriginalFilename();
        String namefilepdf = pdffile.getOriginalFilename();
        // get id book need edit
        int id = idbook;
        // select last id table book_img
        Book_img lastbook = this.bookService.thelastimg();
        // get id select + 10 ;
        int thelastidimg = lastbook.getId() + 10;
        // get param info need edit
        String descriptionedit = request.getParameter("descriptionedit");
        String name = request.getParameter("nameedit");
        String year = request.getParameter("yearedit");
        String author = request.getParameter("authoredit");
        if (!"".equals(namefilepdf)) {
            //if file pdf not null
            String dirFilePdf = request.getServletContext().getRealPath("/uploaded/pdf/");
            //create folder if folder not existed yet
            File filepdf = new File(dirFilePdf);
            if (!filepdf.exists()) {
                filepdf.mkdir();
            }
            try {
                // transfer file to folder at project
                pdffile.transferTo(new File(filepdf + File.separator + namefilepdf));
                logger.info("import pdf success");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.error("cannot upfile pdf" + e.getMessage());
            }
            // transmission  link file folder pdf upload && img
            String dirFileImgPdf = request.getServletContext().getRealPath("/uploaded/pdf/extract/");
            String img = request.getServletContext().getRealPath("/resources/img/extract/");
            String PathImg = dirFilePdf + namefilepdf;
            // if status = 0 => go to edit
            int status = 0;
            // create folder save file img
            File fileextract = new File(dirFileImgPdf);
            if (!fileextract.exists()) {
                fileextract.mkdir();
            }
            // remove file img in table Book_img
            bookService.rmImgBook(id);
            // transmission to last id img to controller pdf
            pdff.getid(thelastidimg);
            // call controller pdf => function render extract img and save img in table book_img
            pdff.getImagePDF(PathImg, dirFileImgPdf, img, id, status);
        }

        if (!"".equals(nameFile2)) {
            // if file img not null && upload file img
            String dirFile = request.getServletContext().getRealPath("/uploaded/img/");
            System.out.println("read path :" + dirFile);
            String pdf = "/uploaded/pdf/" + namefilepdf;
            File fileDir = new File(dirFile);
            if (!fileDir.exists()) {
                fileDir.mkdir();
            }
            try {
                commonsMultipartFiles.transferTo(new File(fileDir + File.separator + nameFile2));
                String img = "/resources/img/" + nameFile2;
                Book book = new Book(name, year, author, img, id, pdf, descriptionedit);
                // update book
                this.bookService.updateBook(book);
                logger.info("import : "+ nameFile2 +  " success");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.error("cannot upfile img " + nameFile2 + "  : : :" + e.getMessage());
            }
        }
        //if img null --> select extract img 1 to do  book avatar
        else if (!"".equals(namefilepdf) && nameFile2 == "") {
            String dirFile = request.getServletContext().getRealPath("/resources/img/extract/");
            String nameFileNull = "extractimage" + (thelastidimg - 30) + ".png";
            File fileDir = new File(dirFile);
            String pdf = "/uploaded/pdf/" + namefilepdf;
            if (!fileDir.exists()) {
                fileDir.mkdir();
            }
            try {
                String img = "/resources/img/extract/" + nameFileNull;
                Book book = new Book(name, year, author, img, id, pdf, descriptionedit);
                this.bookService.updateBook(book);
                logger.info("import  success ---- img null select img 1 ");

            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.error("cannot upfile img  --- case img null : : :" + e.getMessage());
            }
        }


        return "redirect:/admin";
    }

    @RequestMapping(value = "/admin/delbook")
    public String delbook(HttpServletRequest request, Model model) {
        // if check = 1 is when after delete
        check = 1;
        int id = Integer.parseInt(request.getParameter("id"));
        // save id after delete
        safeiddel = id;
        logger.info("delete book" + id);
        this.bookService.rmBook(id);
        return "redirect:/admin";
    }



    @RequestMapping(value = "/search-name", method = RequestMethod.GET)
    public ResponseEntity<List<Book>> searchByNameAdmin(HttpServletRequest request, HttpServletResponse response,
                                                        Model model, HttpSession session) {

        try {
            List<Book> bookList;
            String name = request.getParameter("namebook");
            // get param name book
            // query book
            bookList = bookService.selectBookName(name);
            logger.info("search book name :" + name);

            return new ResponseEntity<List<Book>>(bookList, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("search book name  fail : "  +e);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping("/single-product")
    public String showProductDetail(HttpServletRequest request, HttpServletResponse response, Model model,
                                    HttpSession session) {
        //get param id product book
        int id = Integer.parseInt(request.getParameter("id"));
        idProduct = id;
        List<String> listImage = bookService.listimg(id);
        model.addAttribute("listimage", listImage);
        Book b = bookService.b(id);
        model.addAttribute("book", b);
        String pdf = bookService.linkpdf(id);
        // query book id && query book_img
        // query rating for book
        List<Rating> showratingid = ratingService.showRatingforid(idProduct);
        if (showratingid.isEmpty()) {
            model.addAttribute("pdf", pdf);
        } else {
            // query count and total  average score rating
            Long count = ratingService.countRating(idProduct);
            Long totalscore = ratingService.totalScore(idProduct);
            double result = totalscore / count;
            double newresult = Math.round(result * 100.0) / 100.0;
            model.addAttribute("countrt", count);
            model.addAttribute("showratingid", showratingid);
            model.addAttribute("showscore", newresult);
            model.addAttribute("pdf", pdf);
        }


        return "single-product";
    }

    @RequestMapping("/admin/add")
    public String add(HttpServletRequest request, HttpServletResponse response, Model model,
                      HttpSession session) {

        return "addbok";
    }

    @RequestMapping(value = "/admin/getid", method = RequestMethod.GET)
    public ResponseEntity<Book> getidbook(HttpServletRequest request, HttpServletResponse response,
                                          Model model, HttpSession session) {
        try {
            // get id select info book show info on tab edit book
            int id = Integer.parseInt(request.getParameter("id"));
            logger.info("get id : " + id);
            idbook = id;
            Book b = bookService.b(id);
            return new ResponseEntity<Book>(b, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("get id fail : " + e);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/view")
    public String viewbook(HttpServletRequest request, Model model) {
        // get param id && show page view &&
        int id = Integer.parseInt(request.getParameter("id"));
        idpdf = id;
        logger.info("view book : " + id);
        String pdf = bookService.linkpdf(id);
        // add link pdf page view
        model.addAttribute("pdf", pdf);
        return "viewbook";
    }


    @RequestMapping(value = "/admin/insert-product", method = RequestMethod.POST)
    public String insertProduct(HttpServletRequest request, Model model, @RequestParam(value = "file2") CommonsMultipartFile commonsMultipartFiles, @RequestParam(value = "filepdf") CommonsMultipartFile pdffile) throws IOException {
        // param info book need for insert
        String nameFile2 = commonsMultipartFiles.getOriginalFilename();
        String namefilepdf = pdffile.getOriginalFilename();
        String descriptionadd = request.getParameter("descriptionadd");
        String name = request.getParameter("nameadd");
        String author = request.getParameter("authoradd");
        String date = request.getParameter("yearadd");
        // select last id book
        Book lastbook = this.bookService.selectlastid();
        // +10 last id  && used for add Book_img
        int id = lastbook.getId() + 10;
        // select last book_img
        Book_img lastimg = this.bookService.thelastimg();
        int thelastidimg = lastimg.getId() + 10;
        // if file pdf not null  => upload file pdf
        if (!"".equals(namefilepdf)) {
            String dirFilePdf = request.getServletContext().getRealPath("/uploaded/pdf/");
            logger.info("path pdf : " + dirFilePdf);
            File filepdf = new File(dirFilePdf);
            if (!filepdf.exists()) {
                filepdf.mkdir();
            }
            try {
                pdffile.transferTo(new File(filepdf + File.separator + namefilepdf));
                logger.info("sucess update pdf" +namefilepdf);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.info("cannot upload file pdf" +namefilepdf);
            }
            String dirFileImgPdf = request.getServletContext().getRealPath("/uploaded/pdf/extract/");
            String img = request.getServletContext().getRealPath("/resources/img/extract/");
            String PathImg = dirFilePdf + namefilepdf;
            int status = 1;
            File fileextract = new File(dirFileImgPdf);
            if (!fileextract.exists()) {
                fileextract.mkdir();
            }
            logger.info( "add book : check = " +check );
            if (check == 0) {
                // check = 0 => go to add book_img with id = last id book
                pdff.getid(thelastidimg);
                pdff.getImagePDF(PathImg, dirFileImgPdf, img, id, status);
                logger.info("add book_img with id last id book");
            } else if (check == 1) {
                if(safeiddel>id){
                    // check = 1 && > id last book ==> go to add book_img with id = safeiddel
                    pdff.getid(thelastidimg);
                    pdff.getImagePDF(PathImg, dirFileImgPdf, img, safeiddel + 10, status);
                    check = 0;
                    logger.info("add book_img with id last id del");
                }else {
                    // check =1 && < id last book ==> go to ad book_img with id = id last
                    pdff.getid(thelastidimg);
                    pdff.getImagePDF(PathImg, dirFileImgPdf, img, id, status);
                    // return check = 0
                    check =0;
                    logger.info("add book_img with id last id book");
                }

            }

        }
       // file img not null => upload img
        if (!"".equals(nameFile2)) {
            String dirFile = request.getServletContext().getRealPath("/uploaded/img/");
            logger.info("path img add book : " + dirFile);
            File fileDir = new File(dirFile);
            String pdf = "/uploaded/pdf/" + namefilepdf;
            if (!fileDir.exists()) {
                fileDir.mkdir();
            }
            try {
                commonsMultipartFiles.transferTo(new File(fileDir + File.separator + nameFile2));
                String img = "/uploaded/img/" + nameFile2;
                Book book = new Book(name, date, author, img, pdf, descriptionadd);
                bookService.addbook(book);
                logger.info("add book Success");

            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.info("add book fail : " + e.getMessage() );
            }
            //if img null --> select page 1 in file pdf
        } else if (!"".equals(namefilepdf) && nameFile2 == "") {
            String dirFile = request.getServletContext().getRealPath("/resources/img/extract/");
            String nameFileNull = "extractimage" + thelastidimg + ".png";
            File fileDir = new File(dirFile);
            String pdf = "/uploaded/pdf/" + namefilepdf;
            if (!fileDir.exists()) {
                fileDir.mkdir();
            }
            try {
                String img = "/resources/img/extract/" + nameFileNull;
                Book book = new Book(name, date, author, img, pdf, descriptionadd);
                bookService.addbook(book);
                logger.info("Success insert book vs img value null !");

            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.info("Upload file Fail vs img value null !");
            }
        }

        return "redirect:/admin";
    }


    @RequestMapping(value = "/addrating", produces = {MimeTypeUtils.APPLICATION_JSON_VALUE}, headers = {
            "Accept=application/json"}, method = RequestMethod.GET)
    public ResponseEntity<List<Rating>> addrating(HttpServletRequest request, HttpServletResponse response, Model model) {
        HttpSession session = request.getSession();
        // get param
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String date = df.format(new Date());
        Account account = (Account) session.getAttribute("accountlogin");
        int scorerating = Integer.parseInt(request.getParameter("scoreid"));
        String email = request.getParameter("email");
        String comment = request.getParameter("comment");
        // split "/" convert => "-"
        String[] words = date.split("/");
        String str1 = words[0];
        String str2 = words[1];
        String str3 = words[2];
        String total = str3 + "-" + str2 + "-" + str1;
        // Anti-spam concept rating
        try {
            if (session.getAttribute("accountlogin") == null) {
                //Check if you are logged in
                logger.info("You are not logged in");
                return new ResponseEntity<List<Rating>>(HttpStatus.BAD_REQUEST);
            } else {
                Rating rating = new Rating(scorerating, email, total, comment, idProduct, account.getId());
                // check account  comment not yet
                List<Integer> listacountid = ratingService.slRatingUser(account.getId());
                // check account  comment product not yet
                List<Rating> slrating = ratingService.slRating_id_and_name(rating);
                if (listacountid.isEmpty()) {
                    // If not, insert it
                    ratingService.insertRating(rating);
                    List<Rating> showratingid = ratingService.showRatingforid(idProduct);
                    logger.info("insert  comment product ");
                    return new ResponseEntity<List<Rating>>(showratingid, HttpStatus.OK);
                } else {
                    if (slrating.isEmpty()) {
                        ratingService.insertRating(rating);
                        // if no cmt product , insert it
                        List<Rating> showratingid = ratingService.showRatingforid(idProduct);
                        logger.info("insert  comment product  with product");
                        return new ResponseEntity<List<Rating>>(showratingid, HttpStatus.OK);
                    } else {
                        ratingService.updateRating(rating);
                        //If  ever commented product => edit
                        List<Rating> showratingid = ratingService.showRatingforid(idProduct);
                        logger.info("insert  comment product  with edit comment product for account");
                        return new ResponseEntity<List<Rating>>(showratingid, HttpStatus.OK);
                    }
                }
            }
        } catch (NullPointerException e) {
            logger.error("error add rating");
            return new ResponseEntity<List<Rating>>(HttpStatus.BAD_REQUEST);
        }

    }


}

