package com.thanh.service;

import com.thanh.model.Rating;

import java.util.List;

public interface RatingService {

    List<Integer> slRatingUser(int account_id);

    void insertRating(Rating rating);

    List<Rating> showRatingforid(int idProduct);

    List<Rating> slRating_id_and_name(Rating rating);

    void updateRating(Rating rating);

    Long countRating(int idProduct);

    Long totalScore(int idProduct);
}
