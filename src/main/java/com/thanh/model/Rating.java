package com.thanh.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Rating implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int score;
    private String email;
    private String date;
    private String comment;
    private int book_id;
    private int account_id;

    public Rating(int score, String email, String date, String comment, int book_id,  int account_id){
        super();
        this.score = score;
        this.email=email;
        this.date=date;
        this.comment = comment;
        this.book_id=book_id;
        this.account_id=account_id;
    }

    public Rating(){
        super();
    }

    public int getId() {
        return id;
    }

    public int getBook_id() {
        return book_id;
    }

    public int getAccount_id() {
        return account_id;
    }

    public int getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }

    public String getDate() {
        return date;
    }

    public String getEmail() {
        return email;
    }

}
