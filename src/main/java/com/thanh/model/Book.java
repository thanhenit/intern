package com.thanh.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String year;
    private String author;
    private String img;
    private String pdf;
    private String description;

    public Book(String name, String year, String author, String img,String pdf,String description){
        super();
        this.name = name;
        this.year=year;
        this.author=author;
        this.img = img;
        this.pdf=pdf;
        this.description=description;
    }

    public Book(String name, String year, String author, String img,int id,String pdf,String description){
        super();
        this.name = name;
        this.year=year;
        this.author=author;
        this.img = img;
        this.id=id;
        this.pdf=pdf;
        this.description=description;
    }
    public Book(){
        super();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getYear() {
        return year;
    }

    public String getAuthor() {
        return author;
    }

    public String getImg() {
        return img;
    }

    public String getPdf() {
        return pdf;
    }

    public String getDescription() {
        return description;
    }
}
