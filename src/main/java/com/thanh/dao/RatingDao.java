package com.thanh.dao;

import com.thanh.model.Rating;

import java.util.List;

public interface RatingDao {

    List<Integer> slRatingforUser(int account_id);

    void insertRating(Rating rating);

    List<Rating> showRatingforid(int idProduct);

    List<Rating> slRating_id_and_name(Rating rating);

    void updateRating(Rating rating);

    Long countRating(int idProduct);

    Long totalScore(int idProdcut);
}
