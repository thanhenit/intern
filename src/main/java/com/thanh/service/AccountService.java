package com.thanh.service;

import com.thanh.model.Account;

public interface AccountService {

    Account checklogin(String username, String psw);
}
