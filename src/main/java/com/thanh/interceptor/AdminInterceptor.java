package com.thanh.interceptor;

import com.thanh.model.Account;
import com.thanh.model.Book;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdminInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        boolean conditions = false;
        HttpSession session = request.getSession();

        if (session.getAttribute("accountlogin") != null) {
            Account acc = (Account) session.getAttribute("accountlogin");
            String uname = acc.getUsername();
            if (uname.equals("admin")) {
                conditions = true;
            } else {
                response.sendRedirect(request.getContextPath() + "/");
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/login");
        }
        return conditions;
    }

}
