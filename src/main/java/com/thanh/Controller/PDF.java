package com.thanh.Controller;

import com.thanh.model.Book_img;
import com.thanh.service.BookService;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.stereotype.Controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.inject.Inject;

@Controller
public class PDF {

    @Inject
     BookService bookService;

    private static final Logger logger = Logger.getLogger(PDF.class);

    private static int n ;
    public void getid(int lastid){
        n=lastid;
    }

    public void getImagePDF(String a, String b, String c, int id,int status) throws IOException {
        File file = new File(a);
        // load file pdf uploaded
        PDDocument document = PDDocument.load(file);
        // render file pdf uploaded
        PDFRenderer renderer = new PDFRenderer(document);


        int j =0;
        for (int i = n; i < n + 31; i += 10) {
            // get img in pdf file && save img file
            BufferedImage img = renderer.renderImage(j);
            ImageIO.write(img, "JPEG", new File(b + "extractimg" + i + ".png"));
            String filea = b + "extractimg" + i + ".png";
            String fileb = c + "extractimage" + i + ".png";
            String pathimg = "/resources/img/extract/" + "extractimage" + i + ".png";
            File source = new File(filea);
            File dest = new File(fileb);
            FileUtils.copyFile(source, dest);
            // add img book for table book_img
            Book_img bimg = new Book_img(pathimg,id);
            this.bookService.addimg(bimg);


            j++;

        }
       logger.info("add img for table book_img ");
        document.close();

    }
}
