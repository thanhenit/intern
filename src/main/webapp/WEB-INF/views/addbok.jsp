<%--
  Created by IntelliJ IDEA.
  User: masterenit
  Date: 18/10/2019
  Time: 09:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Title</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.1.0/react.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.1.0/react-dom.min.js"></script>
</head>
<body>

<script>

    var loadFile2 = function (event) {
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('output2');
            output.src = reader.result;
            console.log(reader);
            console.log(output);
        };
        reader.readAsDataURL(event.target.files[0]);
    };

</script>
<section>
    <div class="container">
        <form class="form-horizontal" action="${pageContext.request.contextPath}/admin/insert-product" method="post"
              enctype="multipart/form-data">
            <!-- File Button -->
            <h1>ADD BOOK</h1>
            <div class="form-group">
                <p>name</p>
                <input type="text" id="nameadd" name="nameadd" placeholder="Name book" required="">
                <p>author</p>
                <input type="text" id="authoradd" name="authoradd" placeholder="Author book" required="">
                <p>year</p>
                <input type="date" id="yearadd" name="yearadd" value="2019-10-21" required="" style="width: 205px;">
                <p>Description</p>
                <textarea name="descriptionadd" id="descriptionadd" rows="10" placeholder="Description"></textarea>
                <p>Image</p>
                <input id="file2" name="file2" class="input-file"
                       type="file" onchange="loadFile2(event)" accept="image/gif, image/jpeg, image/png">
                <img id="output2" src="#" style="height: 100px;width: 100px;"/>
            </div>
            <div>
                <p>PDF File</p>
                <input id="fileppdf" name="filepdf" class="input-file"
                       type="file" accept="application/pdf">
                <div>

                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <input name="submit" value="Add" type="submit" class="btn btn-primary btn-lg"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

</body>
</html>
