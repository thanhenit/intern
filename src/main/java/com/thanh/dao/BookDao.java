package com.thanh.dao;

import com.thanh.model.Book;
import com.thanh.model.Book_img;

import java.util.List;

public interface BookDao {
    List<Book> listbook();

    void addbook(Book b);

    void updateBook(Book b);

    void rmBook(int id);

    List<Book> selectBookName(String name);

    List<String> listimg(int id);

    Book b(int id);

    void addimg(Book_img bimg);

    Book SelectLastBook();

    String linkpdf(int id);

    void updateBookImg(Book_img bing);

    void rmImgBook(int book_id);

    Book_img theLastImg();
}
