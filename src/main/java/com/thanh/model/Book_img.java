package com.thanh.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Book_img implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String img;
    private int Book_id;



    public Book_img(){
        super();

    }
    public Book_img(String img,int Book_id){
        super();
        this.img=img;
        this.Book_id=Book_id;
    }
    public Book_img(int id,String img,int Book_id){
        super();
        this.id=id;
        this.img=img;
        this.Book_id=Book_id;
    }

    public int getId() {
        return id;
    }

    public int getBook_id() {
        return Book_id;
    }

    public String getImg() {
        return img;
    }
}
